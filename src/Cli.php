<?php
/**
 *  __  __      ___  ___  _      ___          _               __       ___        _
 * |  \/  |_  _/ __|/ _ \| |    | _ ) __ _ __| |___  _ _ __  / _|___  | _ \___ __| |_ ___ _ _ ___
 * | |\/| | || \__ \ (_) | |__  | _ \/ _` / _| / / || | '_ \ > _|_ _| |   / -_|_-<  _/ _ \ '_/ -_)
 * |_|  |_|\_, |___/\__\_\____| |___/\__,_\__|_\_\\_,_| .__/ \_____|  |_|_\___/__/\__\___/_| \___|
 *         |__/                                       |_|
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\MySQLBackupRestore;

/**
 * Class Cli
 * @package Rodziu\MySQLBackupRestore
 */
class Cli{
	/**
	 * @var \PDO
	 */
	private $pdo;
	/**
	 * @var bool
	 */
	private $verbose;

	/**
	 * Cli constructor.
	 *
	 * @param \PDO $pdo
	 * @param bool $verbose
	 */
	public function __construct(\PDO $pdo, bool $verbose){
		$this->pdo = $pdo;
		$this->verbose = $verbose;
	}

	/**
	 */
	public static function main(){
		$options = self::getOptions($_SERVER['argv']);
		if(empty($options)){
			self::showHelp();
			exit;
		}
		try{
			$pdo = new \PDO("mysql:host={$options['host']};charset=utf8mb4", $options['user'], $options['password'],
				[
					\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
					\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
				]
			);
		}catch(\PDOException $e){
			echo "Error when trying to connect to database. Message was: {$e->getMessage()}\n";
			exit;
		}
		$cli = new Cli($pdo, true);
		$cli->createBackup($options['objects'], $options['dumpFilePath']);
		exit;
	}

	/**
	 * @param array $argv
	 *
	 * @return array
	 */
	public static function getOptions(array $argv): array{
		$options = getopt("u:p::h:", ["user::", "password::", "host:"], $idx);
		if(!isset($argv[$idx]) || !isset($argv[$idx + 1])){
			return [];
		}
		$dumpFilePath = $argv[$idx];
		$objects = ['db' => []];
		$argc = count($argv);
		$dbOption = $argv[$idx + 1];
		switch($dbOption){
			case '--databases':
				if(!isset($argv[$idx + 2])){
					return [];
				}
				for($i = $idx + 2; $i < $argc; $i++){
					$objects['db'][] = $argv[$i];
				}
				break;
			default:
				$database = $dbOption;
				$objects['db'][$database] = [];
				for($i = $idx + 2; $i < $argc; $i++){
					$objects['db'][$database][] = $argv[$i];
				}
				break;
		}
		//
		$ret = [
			'host'         => 'localhost',
			'user'         => 'root',
			'password'     => '',
			'dumpFilePath' => $dumpFilePath,
			'objects'      => $objects
		];
		foreach($options as $k => $v){
			switch($k){
				case 'h':
					$k = 'host';
					break;
				case 'u':
					$k = 'user';
					break;
				case 'p':
					$k = 'password';
					break;
				default:
					break;
			}
			if(is_string($v)){
				$ret[$k] = $v;
			}
		}
		return $ret;
	}

	/**
	 */
	public static function showHelp(){
		echo "Usage:\t{$_SERVER['argv'][0]} [OPTIONS] /path/to/backup.phar database [objects]\n"
			."OR\t{$_SERVER['argv'][0]} [OPTIONS] /path/to/backup.phar --databases DB1 [DB2 DB3...]\n\n"
			."OPTIONS:\n"
			."-h, --host\tHost used in connection (defaults to localhost)\n"
			."-u, --user\tUser used in connection (defaults to root)\n"
			."-p, --password\tPassword used in connection (defaults to no password)\n";
	}

	/**
	 * Create backup at $dumpFilePath for provided $objects.
	 *
	 * @param array $objects
	 * @param string $dumpFilePath
	 * @param string|null $defaultDatabase
	 *
	 * @return bool - was backup properly created?
	 */
	public function createBackup(array $objects, string $dumpFilePath, string $defaultDatabase = null): bool{
		$backup = new Backup($this->pdo, $defaultDatabase, $this->verbose);
		$objects = $backup->parseObjectsArray($objects);
		$dbObjects = $backup->getDbObjects(array_keys($objects));
		$backupList = $backup->getBackupList($dbObjects, $objects);
		if(isset($backupList['unrecognized'])){
			$response = false;
			if($this->verbose){
				$backup->display(
					"Some objects were not found in MySQL: ".implode(", ", $backupList['unrecognized'])."."
				);
				unset($backupList['unrecognized']);
				if(!empty($backupList)){
					echo "Do You want to skip them in backup or abort? [y/n] ";
					$response = Restore::cin();
					$response = $response == 'Y' || $response == 'y';
				}else{
					$backup->display('No valid objects are available for backup, aborting.');
				}
			}
			if(!$response){
				return $response;
			}
		}
		$backup->createDump($backupList, $dumpFilePath);
		$backup->display("Backup file created in $dumpFilePath");
		return true;
	}
}