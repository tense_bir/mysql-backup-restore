<?php
/**
 *  __  __      ___  ___  _      ___          _               __       ___        _
 * |  \/  |_  _/ __|/ _ \| |    | _ ) __ _ __| |___  _ _ __  / _|___  | _ \___ __| |_ ___ _ _ ___
 * | |\/| | || \__ \ (_) | |__  | _ \/ _` / _| / / || | '_ \ > _|_ _| |   / -_|_-<  _/ _ \ '_/ -_)
 * |_|  |_|\_, |___/\__\_\____| |___/\__,_\__|_\_\\_,_| .__/ \_____|  |_|_\___/__/\__\___/_| \___|
 *         |__/                                       |_|
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\MySQLBackupRestore;

/**
 * Class MySQLBackup
 * @package Rodziu
 */
class Backup{
	const DB_OBJECTS = ['table', 'view', 'procedure', 'function', 'trigger', 'event'];
	/**
	 * @var int
	 */
	public static $exportThrottle = 500;
	/**
	 * @var \PDO
	 */
	private $pdo;
	/**
	 * @var null|string
	 */
	private $defaultDatabase = null;
	/**
	 * @var bool
	 */
	private $verbose = false;

	/**
	 * MySQLBackup constructor.
	 *
	 * @param \PDO $pdo
	 * @param string|null $defaultDatabase
	 * @param bool $verbose
	 */
	public function __construct(\PDO $pdo, string $defaultDatabase = null, bool $verbose = false){
		$this->pdo = $pdo;
		$this->defaultDatabase = $defaultDatabase;
		$this->verbose = $verbose;
	}

	/**
	 * Parse database objects array provided by user.
	 *
	 * @param array $objects
	 *
	 * @return array
	 */
	public function parseObjectsArray(array $objects){
		if(empty($objects)){
			if(!is_null($this->defaultDatabase)){
				return [$this->defaultDatabase => []];
			}else{
				return [];
			}
		}
		$databases = [];
		$parse = function($k, $o, $db) use (&$databases){
			if(!isset($databases[$db])){
				$databases[$db] = [];
			}
			if(!in_array($k, self::DB_OBJECTS) && !is_numeric($k)){
				throw new MySQLBackupException("Unrecognized key `$k` in db objects definition!");
			}else if(is_numeric($k)){
				$k = 'detect';
			}
			if(empty($o)){
				if($k == 'detect'){
					return;
				}
			}
			if(!is_iterable($o)){
				$o = [$o];
			}
			if(!isset($databases[$db][$k])){
				$databases[$db][$k] = [];
			}
			$databases[$db][$k] = array_merge($databases[$db][$k], is_array($o) ? $o : [$o]);
		};
		// iterate first level
		foreach($objects as $k => $o){
			if($k === 'db'){
				if(is_iterable($o)){ // 'db' => [...]
					foreach($o as $dk => $do){
						if(is_numeric($dk)){ // 'db' => ['db_name']
							$parse(0, [], $do);
						}else{ // 'db' => ['db_name' => [...]]
							$parse(0, [], $dk); // in case of 'db' => ['db_name' => []]
							if(!is_array($do)){
								throw new MySQLBackupException(
									"Database objects to backup should be provided in an array, string given ($dk => $do)"
								);
							}
							foreach($do as $nk => $no){
								$parse($nk, $no, $dk);
							}
						}
					}
				}else{ // 'db' => 'db_name'
					$parse(0, [], $o);
				}
			}else if(!is_null($this->defaultDatabase)){
				$parse($k, $o, $this->defaultDatabase);
			}
		}
		return $databases;
	}

	/**
	 * Get all objects for each of provided databases.
	 *
	 * @param array|null $dataBases
	 *
	 * @return array
	 */
	public function getDbObjects(array $dataBases = null){
		if(is_null($dataBases) && !is_null($this->defaultDatabase)){
			$dataBases = [$this->defaultDatabase];
		}
		$ret = [];
		foreach($dataBases as $db){
			try{
				$tables = $views = [];
				$data = $this->select("show table status from $db");
				foreach($data as $d){
					if($d['Comment'] == 'VIEW'){
						$views[] = $d['Name'];
					}else{
						$tables[] = $d['Name'];
					}
				}
				$triggers = array_column($this->select(
					"show triggers from $db"
				), 'Trigger');
				$ret[$db] = [
					'table'     => $tables,
					'view'      => $views,
					'procedure' => array_column($this->select(
						"show procedure status where Db=:db", [
							':db' => $db
						]
					), 'Name'),
					'function'  => array_column($this->select(
						"show function status where Db=:db", [
							':db' => $db
						]
					), 'Name'),
					'trigger'   => $triggers,
					'event'     => array_column($this->select(
						"show events from $db"
					), 'Name')
				];
			}catch(\PDOException $e){
				throw new MySQLBackupException("Could not get objects from database $db");
			}
		}
		return $ret;
	}

	/**
	 * @param string $query
	 * @param array $parameters
	 *
	 * @return array
	 */
	private function select(string $query, array $parameters = []): array{
		$statement = $this->pdo->prepare($query);
		if(!empty($parameters)){
			foreach($parameters as $parameter => $value){
				$statement->bindParam($parameter, $value);
			}
		}
		$statement->execute();
		$ret = [];
		if($statement->rowCount()){
			while(($r = $statement->fetch(\PDO::FETCH_ASSOC)) !== false){
				$ret[] = $r;
			}
		}
		return $ret;
	}

	/**
	 * Compare array of objects to backup with array of available database objects.
	 *
	 * @param array $dbObjects
	 * @param array $objectsToBackup
	 *
	 * @return array
	 */
	public function getBackupList(array $dbObjects, array $objectsToBackup): array{
		$ret = [];
		foreach($objectsToBackup as $db => $o){
			if(empty($o)){ // dump whole database
				$ret[$db] = $dbObjects[$db];
			}else{
				$ret[$db] = [];
				foreach($o as $type => $objects){
					if($type == 'detect'){
						foreach($objects as $object){
							$detected = false;
							foreach($dbObjects[$db] as $type => $dbO){
								if(in_array($object, $dbO)){
									if(!isset($ret[$db][$type])){
										$ret[$db][$type] = [];
									}
									$ret[$db][$type][] = $object;
									$detected = true;
									break;
								}
							}
							if(!$detected){
								if(!isset($ret['unrecognized'])){
									$ret['unrecognized'] = [];
								}
								$ret['unrecognized'][] = "$db.$object";
							}
						}
					}else{
						if(!isset($dbObjects[$db][$type])){
							throw new MySQLBackupException("Unrecognized object type $type in database $db");
						}
						foreach($objects as $object){
							if(!in_array($object, $dbObjects[$db][$type])){
								if(!isset($ret['unrecognized'])){
									$ret['unrecognized'] = [];
								}
								$ret['unrecognized'][] = "$type $db.$object";
							}
						}
						$ret[$db][$type] = $objects;
					}
				}
				if(empty($ret[$db])){
					unset($ret[$db]);
				}
			}
		}
		return $ret;
	}

	/**
	 * @param array $backupList
	 * @param string $dumpFilePath
	 * @param string|null $dbHost
	 * @param string|null $dbUser
	 * @param string|null $dbPass
	 */
	public function createDump(
		array $backupList, string $dumpFilePath, string $dbHost = null, string $dbUser = null, string $dbPass = null
	){
		$umask = umask(0);
		@unlink($dumpFilePath);
		// region phar
		if((bool)ini_get('phar.readonly')){
			$this->display("Phar creation is disabled in php.ini. This script will try to override it using a sub process.");
			$tmpPath = sys_get_temp_dir().DIRECTORY_SEPARATOR.'mysql-backup-restore';
			$rmTmpPath = function() use ($tmpPath){
				if(!is_dir($tmpPath)){
					return;
				}
				$iterator = new \RecursiveIteratorIterator(
					new \RecursiveDirectoryIterator($tmpPath, \RecursiveDirectoryIterator::SKIP_DOTS),
					\RecursiveIteratorIterator::CHILD_FIRST
				);
				foreach($iterator as $i){
					/** @var $i \SplFileInfo */
					if($i->isDir()){
						@rmdir($i->getPathname());
					}else{
						@unlink($i->getPathname());
					}
				}
				@rmdir($tmpPath);
			};
			$rmTmpPath();
			mkdir($tmpPath);
			$addToPhar = function(string $localName, string $contents) use ($tmpPath){
				$path = "$tmpPath/$localName";
				@mkdir(dirname($path), 0777, true);
				file_put_contents($path, $contents);
			};
			$createPhar = function() use ($dumpFilePath, $tmpPath, $rmTmpPath): bool{
				$pharCreator = sys_get_temp_dir().DIRECTORY_SEPARATOR.'mysql-backup-restore.php';
				file_put_contents($pharCreator, '<?php
				try{
					$phar = new \Phar("'.$dumpFilePath.'");
					$phar->startBuffering();
					$phar->setStub(
						"#!/usr/bin/env php\n".$phar->createDefaultStub(\'index.php\')
					);
					$phar->stopBuffering();
					$phar->buildFromDirectory("'.$tmpPath.'");
					$phar->addFile("'.__DIR__.'/Restore.php", \'Restore.php\');
					@unlink(__FILE__);
				}catch(\Throwable $e){
					exit(1);
				}
				');
				exec("/usr/bin/env php -d phar.readonly=0 $pharCreator", $output, $code);
				$rmTmpPath();
				return $code == 0 && file_exists($dumpFilePath);
			};
		}else{
			$phar = new \Phar($dumpFilePath);
			$phar->startBuffering();
			$phar->setStub(
				"#!/usr/bin/env php\n".$phar::createDefaultStub('index.php')
			);
			$phar->stopBuffering();
			$phar->addFile(__DIR__."/Restore.php", 'Restore.php');
			$addToPhar = function(string $localName, string $contents) use ($phar){
				$phar->addFromString($localName, $contents);
			};
			$createPhar = function(): bool{
				return true;
			};
		}
		$addToPhar('index.php', '<?php
			namespace Rodziu\MySQLBackupRestore;
			require_once __DIR__."/Restore.php";
			Restore::main("'.$dbHost.'", "'.$dbUser.'", "'.$dbPass.'", __DIR__, '.var_export($backupList, true).');		
		');
		// endregion phar
		$backup = new Backup($this->pdo);
		foreach($backupList as $db => $o){
			$this->display("Dumping db $db");
			foreach($o as $type => $objects){
				foreach($objects as $object){
					$this->display("Dumping db $db $type $object");
					$addToPhar("$db/$type/$object.struct", $backup->dumpObjectStructure($object, $type, $db));
					if($type == 'table'){
						$this->display("Dumping db $db $type $object data");
						$data = $backup->dumpTableData($object, $db);
						if(!empty($data)){
							$addToPhar("$db/$type/{$object}.data", $data);
						}
					}
				}
			}
		}
		$db = null;
		foreach($this->backupListIterator($backupList) as $item){
			if($db != $item['db']){
				$this->display("Dumping db {$item['db']}");
				$db = $item['db'];
			}
			$this->display("Dumping db $db {$item['type']} {$item['object']}");
			$addToPhar(
				"$db/{$item['type']}/{$item['object']}.struct",
				$backup->dumpObjectStructure($item['object'], $item['type'], $db)
			);
			if($item['type'] === 'table'){
				$this->display("Dumping db $db {$item['type']} {$item['object']} data");
				$data = $backup->dumpTableData($item['object'], $db);
				if(!empty($data)){
					$addToPhar("$db/{$item['type']}/{$item['object']}.data", $data);
				}
			}
		}
		if(!$createPhar()){
			umask($umask);
			throw new MySQLBackupException("Phar creation is disabled in php.ini. You need to set `phar.readonly` option to `Off`.");
		}
		chmod($dumpFilePath, 0777);
		umask($umask);
	}

	/**
	 * @param string $text
	 */
	public function display(string $text){
		if($this->verbose){
			echo $text.PHP_EOL;
		}
	}

	/**
	 * Dump single object structure.
	 *
	 * @param string $object
	 * @param string $type
	 * @param string $db
	 *
	 * @return string - MySQL query
	 */
	public function dumpObjectStructure(string $object, string $type, string $db){
		if(!in_array($type, self::DB_OBJECTS)){
			throw new MySQLBackupException("Unknown object type $type!");
		}
		$key = 'Create '.ucfirst($type);
		if($type == 'view'){
			$type = 'table';
		}else if($type == 'trigger'){
			$key = 'SQL Original Statement';
		}
		$query = $this->select("show create $type $db.$object")[0][$key];
		$query = preg_replace('#\sDEFINER=`[^`]+`@`[^`]+`#', '', $query);
		return $query;
	}

	/**
	 * @param string $table
	 * @param string $db
	 *
	 * @return string
	 */
	public function dumpTableData(string $table, string $db): string{
		$offset = 0;
		$ret = '';
		do{
			$data = $this->select(
				"select * from $db.$table limit $offset, ".self::$exportThrottle
			);
			if(!empty($data)){
				if($offset == 0){
					$headers = array_keys($data[0]);
					foreach($headers as &$h){
						$h = "`$h`";
					}
					unset($h);
					$ret .= '('.implode(', ', $headers).')';
				}
				foreach($data as $d){
					$ret .= "\n".$this->formatExportRow($d);
				}
				$offset += self::$exportThrottle;
			}
		}while(!empty($data));
		return $ret;
	}

	/**
	 * @param array $row
	 *
	 * @return string
	 */
	private function formatExportRow(array $row): string{
		foreach($row as &$r){
			if(is_numeric($r)){
				continue;
			}else if(is_null($r)){
				$r = 'null';
			}else{
				$r = addslashes($r);
				$r = str_replace(["\r", "\n"], ["\\r", "\\n"], $r);
				$r = "'".$r."'";
			}
		}
		unset($r);
		return '('.implode(', ', $row).')';
	}

	/**
	 * @param array $backupList
	 *
	 * @return \Generator
	 */
	public function backupListIterator(array $backupList): \Generator{
		foreach($backupList as $db => $o){
			foreach($o as $type => $objects){
				foreach($objects as $object){
					yield [
						"db"     => $db,
						"object" => $object,
						"type"   => $type,
						"query"  => $this->dumpObjectStructure($object, $type, $db)
					];
				}
			}
		}
	}

	/**
	 * @param string $objectStructureQuery
	 * @param string $sourceDataBase
	 * @param string $targetDataBase
	 *
	 * @return string
	 */
	public function alterTargetDatabase(string $objectStructureQuery, string $sourceDataBase, string $targetDataBase): string{
		$objectStructureQuery = str_replace(
			["`$sourceDataBase`.", " $sourceDataBase."], " `$targetDataBase`.", $objectStructureQuery
		);
		return $objectStructureQuery;
	}
}