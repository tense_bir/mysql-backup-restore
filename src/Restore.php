<?php
/**
 *  __  __      ___  ___  _      ___          _               __       ___        _
 * |  \/  |_  _/ __|/ _ \| |    | _ ) __ _ __| |___  _ _ __  / _|___  | _ \___ __| |_ ___ _ _ ___
 * | |\/| | || \__ \ (_) | |__  | _ \/ _` / _| / / || | '_ \ > _|_ _| |   / -_|_-<  _/ _ \ '_/ -_)
 * |_|  |_|\_, |___/\__\_\____| |___/\__,_\__|_\_\\_,_| .__/ \_____|  |_|_\___/__/\__\___/_| \___|
 *         |__/                                       |_|
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\MySQLBackupRestore;

/**
 * Class MySQLRestore
 * @package JFramework\Patches
 */
class Restore{
	/**
	 * @var \PDO
	 */
	private $pdo;
	/**
	 * @var string
	 */
	private $basedir;

	/**
	 * SqlRestore constructor.
	 *
	 * @param \PDO $pdo
	 * @param string $basedir
	 */
	public function __construct(\PDO $pdo, string $basedir){
		$this->pdo = $pdo;
		$this->basedir = $basedir;
	}

	// region phar control

	/**
	 * @param string $host
	 * @param string $user
	 * @param string $password
	 * @param string $basedir
	 * @param array $backupList
	 */
	public static function main(string $host, string $user, string $password, string $basedir, array $backupList){
		if(empty($backupList)){
			echo "Backup list is empty, aborting.".PHP_EOL;
			exit(1);
		}
		$pdo = self::connect($host, $user, $password);
		$sqlRestore = new self($pdo, $basedir);
		echo "Do you want to restore whole backup? [y/n] ";
		$response = self::cin();
		$selectiveMode = !($response == 'y' || $response == 'Y');
		/**
		 * @param string $question
		 *
		 * @return bool
		 */
		$shouldRestore = function(string $question) use ($selectiveMode){
			if(!$selectiveMode){
				return true;
			}
			echo "$question? [y/n] ";
			$response = self::cin();
			return $response == 'y' || $response == 'Y';
		};
		/**
		 * @param string $message
		 * @param callable $restoreFn
		 */
		$restore = function(string $message, callable $restoreFn) use ($sqlRestore){
			echo "\r$message";
			try{
				/** @noinspection PhpParamsInspection */
				$restoreFn($sqlRestore);
				echo "\r$message\e[92m ok\e[0m".PHP_EOL;
			}catch(\Exception $e){
				echo "\r$message\e[91m failed!\e[0m".PHP_EOL;
				echo "Error message was: ".$e->getMessage().PHP_EOL;
				echo "Aborting".PHP_EOL;
				exit(1);
			}
		};
		//
		foreach($backupList as $db => $types){
			// restore db
			if($shouldRestore("Restore database `$db`")){
				$restore("Restoring database `$db`", function(Restore $sqlRestore) use ($db){
					$sqlRestore->restoreDatabase($db);
				});
			}
			foreach($types as $type => $objects){
				foreach($objects as $object){
					// restore structure
					if($shouldRestore("Restore $type `$db.$object`")){
						$restore("Restoring $type `$db.$object`", function(Restore $sqlRestore) use ($db, $type, $object){
							$sqlRestore->restoreStructure($db, $type, $object);
						});
					}
					// restore table data
					if($type == 'table' && $sqlRestore->hasData($db, $object) && $shouldRestore("Restore $type `$db.$object` data")){
						$restore("Restoring $type data `$db.$object`", function(Restore $sqlRestore) use ($db, $object){
							$sqlRestore->restoreData($db, $object);
						});
					}
				}
			}
		}
		echo "All done!".PHP_EOL;
	}

	/**
	 * @param string $host
	 * @param string $user
	 * @param string $password
	 *
	 * @return \PDO
	 */
	protected static function connect(string $host, string $user, string $password): \PDO{
		try{
			return new \PDO("mysql:host=$host;charset=utf8mb4", $user, $password,
				[
					\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
					\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION FOREIGN_KEY_CHECKS = 0;'
				]
			);
		}catch(\PDOException $e){
			echo "Could not connect to MySQL using given credentials.".PHP_EOL;
			echo "Error message was: ".$e->getMessage().PHP_EOL;
			echo "Do you want to enter new credentials? [y/n] ";
			$response = self::cin();
			if($response == 'y' || $response == 'Y'){
				echo "MySQL host (was $host): ";
				$host = self::cin();
				echo "User (was $user): ";
				$user = self::cin();
				echo "Password (was $password): ";
				$password = self::cin();
				return self::connect($host, $user, $password);
			}else{
				echo "Aborting.".PHP_EOL;
				exit(1);
			}
		}
	}

	/**
	 * @return string
	 */
	public static function cin(): string{
		$stdin = fopen('php://stdin', 'r');
		$code = trim(fgets($stdin));
		fclose($stdin);
		return $code;
	}
	// endregion phar control

	/**
	 * @param string $db
	 */
	public function restoreDatabase(string $db){
		$this->pdo->prepare("create database if not exists `$db`")->execute();
	}

	/**
	 * @param string $db
	 * @param string $type
	 * @param string $object
	 */
	public function restoreStructure(string $db, string $type, string $object){
		if(file_exists("$this->basedir/$db/$type/$object.struct")){
			$this->pdo->prepare("use `$db`")->execute();
			$this->pdo->prepare("drop $type if exists `$object`")->execute();
			$query = file_get_contents("$this->basedir/$db/$type/$object.struct");
			$this->pdo->prepare($query)->execute();
		}else{
			throw new \RuntimeException("Could not find object structure for $type $db.$object in backup file.");
		}
	}

	/**
	 * @param string $db
	 * @param string $table
	 *
	 * @return bool
	 */
	public function hasData(string $db, string $table): bool{
		return file_exists("$this->basedir/$db/table/$table.data");
	}

	/**
	 * @param string $db
	 * @param string $table
	 * @param int $throttle
	 */
	public function restoreData(string $db, string $table, int $throttle = 500){
		if(($file = fopen("$this->basedir/$db/table/$table.data", 'r'))){
			$i = 0;
			/** @noinspection SqlResolve */
			$insertFormat = "insert into `$db`.`$table`";
			$insertData = [];
			while(($line = fgets($file)) !== false){
				$line = trim($line);
				if($i == 0){
					$insertFormat .= $line." values ";
				}else{
					$insertData[] = $line;
					if($i % $throttle == 0){
						$this->pdo->prepare($insertFormat.implode(",", $insertData))->execute();
						$insertData = [];
					}
				}
				$i++;
			}
			if(!empty($insertData)){
				$this->pdo->prepare($insertFormat.implode(",", $insertData))->execute();
			}
			fclose($file);
		}else{
			throw new \RuntimeException("Could not open table data file for $db.$table.");
		}
	}
}