<?php
/**
 *  __  __      ___  ___  _      ___          _               __       ___        _
 * |  \/  |_  _/ __|/ _ \| |    | _ ) __ _ __| |___  _ _ __  / _|___  | _ \___ __| |_ ___ _ _ ___
 * | |\/| | || \__ \ (_) | |__  | _ \/ _` / _| / / || | '_ \ > _|_ _| |   / -_|_-<  _/ _ \ '_/ -_)
 * |_|  |_|\_, |___/\__\_\____| |___/\__,_\__|_\_\\_,_| .__/ \_____|  |_|_\___/__/\__\___/_| \___|
 *         |__/                                       |_|
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\MySQLBackupRestore;

/**
 * Class BackupTest
 * @package Rodziu\MySQLBackupRestore
 */
class BackupTest extends TestBase{
	/**
	 * @var Backup
	 */
	private $backup;

	/**
	 *
	 */
	protected function setUp(){
		parent::setUp();
		$this->backup = new Backup(self::$pdo, '__mbr_test');
	}

	/**
	 * @dataProvider parseObjectsProvider
	 *
	 * @param array $objects
	 * @param array $expected
	 */
	public function testParseObjectsArray(array $objects, array $expected = null){
		if(is_null($expected)){
			$this->expectException(MySQLBackupException::class);
		}
		$ret = $this->backup->parseObjectsArray($objects);
		static::assertEquals($expected, $ret);
	}

	/**
	 * @return array
	 */
	public function parseObjectsProvider(): array{
		return [
			[[], ['__mbr_test' => []]],
			[
				['sessions', 'users'],
				['__mbr_test' => ['detect' => ['sessions', 'users']]]
			],
			[
				['table' => ['sessions', 'users'], 'view' => 'some_view'],
				['__mbr_test' => ['table' => ['sessions', 'users'], 'view' => ['some_view']]]
			],
			[
				['sessions', 'users', 'db' => '__mbr_test2'],
				['__mbr_test' => ['detect' => ['sessions', 'users']], '__mbr_test2' => []]
			],
			[
				['db' => ['__mbr_test']],
				['__mbr_test' => []]
			],
			[
				['db' => ['__mbr_test' => []]],
				['__mbr_test' => []]
			],
			[
				['db' => ['__mbr_test' => ['some_table']]],
				['__mbr_test' => ['detect' => ['some_table']]]
			],
			[
				['db' => ['__mbr_test' => 'some_table']],
				null
			]
		];
	}

	/**
	 */
	public function testGetDbObjects(){
		$objects = $this->backup->getDbObjects(['__mbr_test']);
		static::assertSame([
			'__mbr_test' => [
				'table'     => [
					'some_table',
					'some_table2'
				],
				'view'      => ['some_view'],
				'procedure' => ['some_procedure'],
				'function'  => ['some_function'],
				'trigger'   => ['some_trigger'],
				'event'     => ['some_event']
			]
		], $objects);
		return $objects;
	}

	/**
	 * @dataProvider backupListProvider
	 *
	 * @param array $objectsToBackup
	 * @param array|null $expected
	 */
	public function testGetBackupList(array $objectsToBackup, array $expected = null){
		$dbObjects = $this->backup->getDbObjects(['__mbr_test']);
		if(is_null($expected)){
			$this->expectException(MySQLBackupException::class);
		}
		$ret = $this->backup->getBackupList($dbObjects, $objectsToBackup);
		static::assertEquals($expected, $ret, print_r($ret, true));
	}

	/**
	 * @return array
	 */
	public function backupListProvider(): array{
		return [
			[
				['__mbr_test' => ['table' => ['some_table']]],
				['__mbr_test' => ['table' => ['some_table']]]
			],
			[
				['__mbr_test' => ['some weird shit' => []]],
				null
			],
			[
				['__mbr_test' => []],
				['__mbr_test' => [
					'table'     => [
						'some_table',
						'some_table2'
					],
					'view'      => ['some_view'],
					'procedure' => ['some_procedure'],
					'function'  => ['some_function'],
					'trigger'   => ['some_trigger'],
					'event'     => ['some_event']
				]]
			],
			[
				['__mbr_test' => ['detect' => ['some_table', 'some_trigger', 'some_view']]],
				['__mbr_test' => ['table' => ['some_table'], 'view' => ['some_view'], 'trigger' => ['some_trigger']]]
			],
			[
				['__mbr_test' => ['detect' => ['something']]],
				['unrecognized' => ['__mbr_test.something']],
			],
		];
	}

	/**
	 * @depends testGetDbObjects
	 *
	 * @param array $dbObjects
	 */
	public function testDumpStructure(array $dbObjects){
		foreach($dbObjects as $db => $o){
			foreach($o as $type => $objects){
				foreach($objects as $object){
					$ret = $this->backup->dumpObjectStructure($object, $type, $db);
					static::assertRegExp('#create.*'.trim($type, 's').'#i', $ret);
				}
			}
		}
	}

	/**
	 */
	public function testDumpData(){
		$dump = $this->backup->dumpTableData('some_table', '__mbr_test');
		$dump = explode("\n", $dump);
		static::assertSame('(`id`, `txt`)', $dump[0]);
		for($i = 1; $i < 1000; $i++){
			if($i == 1){
				$txt = "'some text\\n with\\r\t whitespace'";
			}else if($i == 2){
				$txt = "'some text with \\\" ` \'\' quotes'";
			}else{
				$txt = 'null';
			}
			static::assertSame("($i, $txt)", $dump[$i]);
		}
	}

	/**
	 */
	public function testCreateDump(){
		$dumpPath = sys_get_temp_dir().DIRECTORY_SEPARATOR.'mbr-test.phar';
		$this->backup->createDump($this->backupList, $dumpPath);
		self::assertFileExists($dumpPath);
		@unlink($dumpPath);
	}

	/**
	 */
	public function testAlterTargetDatabase(){
		self::$pdo->prepare('create database __mbr_test_copy')->execute();
		self::$pdo->prepare('use __mbr_test_copy')->execute();
		foreach($this->backup->backupListIterator($this->backupList) as $item){
			$altered = $this->backup->alterTargetDatabase(
				$item['query'], $item['db'], '__mbr_test_copy'
			);
			self::$pdo->prepare($altered)->execute();
			$this->assertObjectExists($item['type'], '__mbr_test_copy', $item['object']);
		}
	}
}
