<?php
/**
 *  __  __      ___  ___  _      ___          _               __       ___        _
 * |  \/  |_  _/ __|/ _ \| |    | _ ) __ _ __| |___  _ _ __  / _|___  | _ \___ __| |_ ___ _ _ ___
 * | |\/| | || \__ \ (_) | |__  | _ \/ _` / _| / / || | '_ \ > _|_ _| |   / -_|_-<  _/ _ \ '_/ -_)
 * |_|  |_|\_, |___/\__\_\____| |___/\__,_\__|_\_\\_,_| .__/ \_____|  |_|_\___/__/\__\___/_| \___|
 *         |__/                                       |_|
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\MySQLBackupRestore;

use \PHPUnit\Framework\TestCase;

/**
 * Class TestBase
 * @package Rodziu\MySQLBackupRestore
 */
class TestBase extends TestCase{
	/**
	 * @var null|\PDO
	 */
	protected static $pdo = null;
	/**
	 * @var array
	 */
	protected $backupList = [
		'__mbr_test' => [
			'table'     => [
				'some_table',
				'some_table2'
			],
			'view'      => ['some_view'],
			'procedure' => ['some_procedure'],
			'function'  => ['some_function'],
			'trigger'   => ['some_trigger'],
			'event'     => ['some_event']
		]
	];

	/**
	 */
	protected function setUp(){
		parent::setUp();
		if(self::$pdo === null){
			self::$pdo = new \PDO(
				"mysql:host={$GLOBALS['DB_HOST']};charset=utf8mb4", $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']
			);
			$this->createTestDB();
			register_shutdown_function(function(){
				self::$pdo->prepare('drop database if exists __mbr_test')->execute();
				self::$pdo->prepare('drop database if exists __mbr_test_copy')->execute();
			});
		}
	}

	/**
	 */
	private function createTestDB(){
		/** @noinspection SqlResolve */
		$queries = [
			'drop database if exists __mbr_test',
			'create database __mbr_test',
			'create table __mbr_test.some_table(id int primary key auto_increment, txt text default null)',
			'create table __mbr_test.some_table2(id int)',
			'create trigger __mbr_test.some_trigger after insert on __mbr_test.some_table for each row insert into __mbr_test.some_table2 values(NEW.id)',
			'create view __mbr_test.some_view as select id from __mbr_test.some_table',
			'create procedure __mbr_test.some_procedure() begin select 1; end',
			'create function __mbr_test.some_function(`in_int` int unsigned) returns int begin return in_int*2; end',
			"create event __mbr_test.some_event on schedule every 1 day starts '2017-07-12 00:00:00' on completion preserve enable do begin select 1; end"
		];
		foreach($queries as $q){
			self::$pdo->prepare($q)->execute();
		}
		self::$pdo->beginTransaction();
		for($i = 1; $i < 1000; $i++){
			if($i == 1){
				$txt = "some text\n with\r\t whitespace";
			}else if($i == 2){
				$txt = "some text with \" ` '' quotes";
			}else{
				$txt = null;
			}
			/** @noinspection SqlResolve */
			$stmt = self::$pdo->prepare(
				"insert into __mbr_test.some_table(id, txt) values($i, :txt)"
			);
			$stmt->bindParam(':txt', $txt, $txt === null ? \PDO::PARAM_NULL : \PDO::PARAM_STR);
			$stmt->execute();
		}
		self::$pdo->commit();
	}

	/**
	 * @param string $type
	 * @param string $db
	 * @param string $object
	 */
	protected function assertObjectExists(string $type, string $db, string $object){
		switch($type){
			case 'table':
			case 'view':
				$query = "show table status from $db where Name = '$object'";
				if($type == 'view'){
					$query .= " and Comment='VIEW'";
				}
				break;
			case 'procedure':
			case 'function':
				$query = "show $type status where Db='$db' and Name='$object'";
				break;
			case 'trigger':
			case 'event':
				$query = "show {$type}s from $db where ";
				if($type == 'trigger'){
					$query .= "`Trigger`='$object'";
				}else{
					$query .= "Name='$object'";
				}
				break;
			default:
				$query = '';
				break;
		}
		$statement = self::$pdo->prepare($query);
		$statement->execute();
		static::assertNotEmpty($statement->fetchAll(), "$db $type");
	}
}