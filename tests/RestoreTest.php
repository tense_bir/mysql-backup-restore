<?php
/**
 *  __  __      ___  ___  _      ___          _               __       ___        _
 * |  \/  |_  _/ __|/ _ \| |    | _ ) __ _ __| |___  _ _ __  / _|___  | _ \___ __| |_ ___ _ _ ___
 * | |\/| | || \__ \ (_) | |__  | _ \/ _` / _| / / || | '_ \ > _|_ _| |   / -_|_-<  _/ _ \ '_/ -_)
 * |_|  |_|\_, |___/\__\_\____| |___/\__,_\__|_\_\\_,_| .__/ \_____|  |_|_\___/__/\__\___/_| \___|
 *         |__/                                       |_|
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\MySQLBackupRestore;

/**
 * Class RestoreTest
 * @package Rodziu\MySQLBackupRestore
 */
class RestoreTest extends TestBase{
	/**
	 * @var Restore
	 */
	private $restore;

	/**
	 */
	protected function setUp(){
		parent::setUp();
		$this->restore = new Restore(self::$pdo,__DIR__.DIRECTORY_SEPARATOR.'restore');
		self::$pdo->prepare('drop database if exists __mbr_test')->execute();
	}

	/**
	 */
	public function testRestoreStructure(){
		foreach($this->backupList as $db => $data){
			$this->restore->restoreDatabase($db);
			foreach($data as $type => $objects){
				foreach($objects as $object){
					$this->restore->restoreStructure('__mbr_test', $type, $object);
					$this->assertObjectExists($type, $db, $object);
				}
			}
		}
	}

	/**
	 * @dataProvider hasDataProvider
	 *
	 * @param string $db
	 * @param string $table
	 * @param bool $ret
	 */
	public function testHasData(string $db, string $table, bool $ret){
		static::assertEquals($ret, $this->restore->hasData($db, $table));
	}

	/**
	 * @return array
	 */
	public function hasDataProvider(): array{
		return [
			['__mbr_test', 'some_table', true],
			['__mbr_test', 'some_table2', true],
			['__mbr_test', 'some', false]
		];
	}

	/**
	 * @depends testRestoreStructure
	 */
	public function testRestoreData(){
		foreach($this->backupList as $db => $data){
			$this->restore->restoreDatabase($db);
			foreach($data as $type => $objects){
				if($type == 'table'){
					foreach($objects as $object){
						if($this->restore->hasData($db, $object)){
							$this->restore->restoreStructure($db, 'table', $object);
							$this->restore->restoreData($db, $object);
							$statement = self::$pdo->prepare("select * from $db.$object");
							$statement->execute();
							static::assertNotEmpty($statement->fetchAll());
						}
					}
				}
			}
		}
	}
}
