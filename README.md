# MySQL Backup & Restore for PHP

Easily create backups of selected MySQL databases or objects (tables, views, procedures, functions, triggers and events). Backups are created as a phar archive that allows you to restore only selected parts of backup.

Backups can be created using PHP API or CLI binary.

## Prerequisites

- PHP 7.1+,
- PDO extension,
- composer :).

## Installing

For single project:

```bash
composer require rodziu/mysql-backup-restore
```

Or globally:

```bash
composer global require rodziu/mysql-backup-restore
```

## Usage as PHP API:

```php
<?php
use Rodziu\MySQLBackupRestore\Backup;
// Backup constructor takes an instance of existing PDO connection as first parameter
$pdo = new \PDO("mysql:host=localhost;charset=utf8mb4");
$backup = new Backup($pdo);
// optionally you can pass $defaultDatabase & $verbose parameters
//
$objects = ['some_table']; // see possible objects definition below
$objects = $backup->parseObjectsArray($objects);
$dbObjects = $backup->getDbObjects(array_keys($objects)); // gets all MySQL objects from provided databases
$backupList = $backup->getBackupList($dbObjects, $objects); // compares possible MySQL objects with objects marked to backup
$backup->createDump($backupList, '/path/to/backup.phar');
```

### Possible objects array definition

You can define objects-to-backup array in one of following ways:

- one-dimensional array of object names (tables, views, procedures, functions, triggers and events) - the script will automatically search them in the database specifying their type,
- multidimensional array with possible keys:
	- table | view | procedure | function | trigger | event - the array of objects of the pre-set type of the key must be given as the value (eg. `['table' => ['table1', 'tabala2']]`)
	- db - then you can enter the names of other databases whose backup is to be performed.

For example:
```php
<?php
['table_name', 'procedure_name']; // backup 'table_name' and 'procedure_name'
['table' => ['name'], 'procedure' => ['name']]; // backup table 'name' and procedure 'name'
['db' => 'database_name']; // backup whole database 'database_name'
['db' => ['database_name']]; // same as above
['db' => ['database_name' => ['table' => 'table_name']]]; // backup table 'table_name' from 'database_name' database
['name', 'db' => 'database_name']; // backup object 'name' from default database and whole database 'database_name'
```

## Usage from cli

```
Usage: ./mysql-backup [OPTIONS] /path/to/backup.phar database [objects]
OR     ./mysql-backup [OPTIONS] /path/to/backup.phar --databases DB1 [DB2 DB3...]

OPTIONS:
-h, --host     Host used in connection (defaults to localhost)
-u, --user     User used in connection (defaults to root)
-p, --password Password used in connection (defaults to no password)

```

## Restoring Backups

Just execute phar archive from CLI. It will ask you which objects you want to restore.